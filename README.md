# jatalbackend

# Jatal 
это новостной портал. В котором вы можете следить за новостями других людей. А также 
создавать собственные новости, которыми вы хотите поделиться с людьми.

Запуск:

1.python -m venv env - виртуальное окружение

2.cd env\Scripts\activate - вход в окружение

3.pip install -r requirements.txt - установка необходимых библиотек.

4.python src\manage.py migrate- запуск миграций

5.python src\manage.py runserver - запуск сервера
